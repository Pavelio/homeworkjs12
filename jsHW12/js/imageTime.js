window.onload = function () {
    let slides = document.querySelectorAll('#wrapper .image-to-show');
    let indexSlides = 0;
    let index = 0
    function nextSlide() {
        if(playing && index<10) index++;
        if (playing && index==10) {
            index=0;
            slides[indexSlides].className = 'image-to-show';
            indexSlides = (indexSlides + 1) % slides.length;
            slides[indexSlides].className = 'image-to-show showing';
        }
    }
    setInterval(nextSlide, 1000);

    let second = 8;
    function tiktak() {
        if (playing) {
            let timer1 = document.getElementById("timer");
            if(second < 10) {second = "0" + second;}
            timer1.innerHTML = second;
            if (second == 0) { second = 10; }
            second--;
        }

    }
    setInterval(tiktak, 1000);

    let miliSecond = 99;
    function timeMili() {
        if (playing) {
            let timer2 = document.getElementById("milisec");
            if(miliSecond < 10) {miliSecond = "0" + miliSecond;}
            timer2.innerHTML = miliSecond;
            if (miliSecond == 1) { miliSecond = 100; }
            miliSecond--;
        }

    }
    setInterval(timeMili, 10);

    let playing = true;
    document.getElementById('play').onclick = function () {
        if (playing === false) {
            playing = true;
        }
    }

    document.getElementById('pause').onclick = function () {
        if (playing === true) {
            playing = false;
        }
    }

}